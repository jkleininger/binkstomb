package net.qrab.binkstomb;

import java.awt.Dimension;

class Gem extends Actor {

  private static final long serialVersionUID = 1L;

  protected Gem(int c, int r) {
    col = c;
    row = r;
  }

  protected Gem(String name) {
    theSprite = new Sprite(name, new Dimension(32, 32));
    theSprite.addFrame(Constants.state.IDLE, 0);
    col = 0;
    row = 0;
  }

}
