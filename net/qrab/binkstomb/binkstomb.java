package net.qrab.binkstomb;

import java.awt.Color;
import java.awt.Container;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

import javax.swing.JApplet;

public class binkstomb extends JApplet {

  private static final long serialVersionUID = 1L;
  Board                     theBoard         = new Board();
  Container                 contentPane;

  Rectangle2D               r                = new Rectangle2D.Double(10, 20, 30, 50);

  public void init() {
  }

  public void start() {
    contentPane = getContentPane();
    contentPane.add(theBoard);
    theBoard.setSize(100, 100);
    System.err.println(theBoard.getSize());
  }

  public void stop() {
  }

  public void paint(Graphics g) {
    paint((Graphics2D) g);
  }

  public void paint(Graphics2D g) {
    g.setColor(new Color(0x002288));
    g.drawRect((int) r.getX(), (int) r.getY(), (int) r.getWidth(), (int) r.getHeight());
    theBoard.paint(g);
  }

}
