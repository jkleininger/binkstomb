package net.qrab.binkstomb;

import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;

public class Sprite {

  private BufferedImage[]                           sheet;
  private Map<Constants.state, List<BufferedImage>> img        = new EnumMap<Constants.state, List<BufferedImage>>(Constants.state.class);

  private Constants.state                           currentState;
  private int                                       currentFrame;

  private String                                    theName;

  private int                                       frameDelay = 100000000;
  private long                                      lastUpdate = System.nanoTime();

  private int                                       wd, ht;

  public Sprite(String theName, Dimension dim) {
    BufferedImage inImg;
    try {
      inImg = ImageIO.read(getClass().getResource("sprites/" + theName + ".png"));
    } catch (IOException ioe) {
      return;
    }
    setName(theName);
    setState(Constants.state.IDLE);
    wd = (int) dim.getWidth();
    ht = (int) dim.getHeight();
    int cols = (int) (inImg.getWidth() / wd);
    int rows = (int) (inImg.getHeight() / ht);
    sheet = new BufferedImage[cols * rows];
    for (int r = 0; r < rows; r++) {
      for (int c = 0; c < cols; c++) {
        sheet[(r * cols) + c] = inImg.getSubimage(c * wd, r * ht, wd, ht);
      }
    }
  }

  Dimension getDimension() {
    return new Dimension(wd, ht);
  }

  void addState(Constants.state s) {
    img.put(s, new ArrayList<BufferedImage>());
  }

  void addFrame(Constants.state s, BufferedImage i) {
    if (img.get(s) == null)
      addState(s);
    img.get(s).add(i);
  }

  void addFrame(Constants.state s, int t) {
    addFrame(s, sheet[t]);
  }

  void addFrames(Constants.state s, int[] tiles) {
    for (int t : tiles)
      addFrame(s, t);
  }

  void addFrames(int[][] tiles) {
    for (int state = 0; state < tiles.length; state++) {
      for (int frame = 0; frame < tiles[state].length; frame++) {
        addFrame(Constants.state.values()[state], tiles[state][frame]);
      }
    }
  }

  BufferedImage getImage() {
    if (currentFrame >= img.get(currentState).size())
      currentFrame = 0;
    return img.get(currentState).get(currentFrame);
  }

  void setState(Constants.state s) {
    currentState = s;
  }

  Constants.state getState() {
    return currentState;
  }

  void setFrame(int fnum) {
    currentFrame = fnum;
  }

  int getFrame() {
    return currentFrame;
  }

  String getName() {
    return theName;
  }

  void setName(String n) {
    theName = n;
  }

  void incFrame() {
    currentFrame = currentFrame + 1 >= img.get(currentState).size() ? 0 : currentFrame + 1;
  }

  void update() {
    if ((System.nanoTime() - lastUpdate) > frameDelay) {
      lastUpdate = System.nanoTime();
      incFrame();
    }
  }

}
