package net.qrab.binkstomb;

import java.awt.Color;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;

abstract class Actor extends Rectangle {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  Color                     theColor;
  Sprite                    theSprite;

  int                       row;
  int                       col;

  Point                     destination;
  int                       step;
  boolean                   atDest;

  protected BufferedImage getImage() {
    return theSprite.getImage();
  }

  protected void getDimensionFromSprite() {
    this.setSize(theSprite.getDimension());
  }

  void updateLocation() {
    if (destination == null)
      return;
    if (this.getLocation().distance(destination.getLocation()) > step) {
      Constants.mapMotion = true;
      double deltaX = destination.getX() - this.getX();
      double deltaY = destination.getY() - this.getY();
      double qbar = Math.sqrt((deltaX * deltaX) + (deltaY * deltaY));
      this.translate((int) ((deltaX * step) / qbar), (int) ((deltaY * step) / qbar));
    } else {
      this.setLocation(destination.getLocation());
      Constants.mapMotion = false;
      atDest = true;
    }
  }

}
